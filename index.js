const USER = process.env.USER || 'Administrator';
const PASSWORD = process.env.PASSWORD || '';
const HOST = process.env.HOST || 'localhost';
const BUCKET = process.env.BUCKET || 'travel-sample';

const DB = require('./db');

const Analytics = {
	async start(db) {
		const qry = 'select * from airports';
		try {
			const results = await db.analyticsQuery(qry);
			console.log('Results:');
			console.log(results);
		} catch(err) {
			console.error(err);
			process.exit(1);
		}
	}
}

const db = new DB({ user: USER, password: PASSWORD, host: HOST, bucket: BUCKET });

db.on('error', err => {
	throw err;
});

db.on('connected', _ => {
	console.log('Couchbase connected');
	Analytics.start(db);
});