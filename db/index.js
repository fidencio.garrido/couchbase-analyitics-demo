const Couchbase = require('couchbase');
const events = require('events');

const connections = {};

function findMissing(source, fields) {
	if (typeof source === 'undefined') {
		return fields;
	}
	if (Array.isArray(fields)) {
		let missing = [];
		fields.forEach( f => {
			if (typeof source[f] === 'undefined') {
				missing.push(f);
			}
		});
		return missing;
	} else return [];
}

class DBConn extends events {
	constructor(options) {
		super();
		const missingFields = findMissing(options, ['user', 'password', 'bucket', 'host']);
		if (missingFields.length > 0) {
			this.emit('error', `INVALID_CONSTRUCTOR - Missing ${missingFields.join(',')}`);
		}
		this.connect(options);
	}

	connect(options) {
		let conn = (typeof connections[options.host] !== 'undefined') ? connections[options.host] : (function() {
			const connection = new Couchbase.Cluster(`couchbase://${options.host}`);
			connection.authenticate(options.user, options.password);
			return connection;
		}());
		this.connection = conn;
		this.bucket = conn.openBucket(options.bucket, (err) => {
			if (err) {
				this.emit('error', err);
				return;
			}
			this.emit('connected');
		});
	}

	analyticsQuery(statement) {
		return new Promise((resolve, reject) => {
			const query = Couchbase.AnalyticsQuery.fromString(statement);
			this.connection.query(query, (error, result) => {
				if(error) {
					reject(error);
					return;
				} else {
					resolve(result);
					return;
				}
			});
		});
	}
}

module.exports = DBConn;