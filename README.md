# Couchbase Analytics

Demo with Couchbase 5.5.

This demo uses updated code as of 12/13/2018, the original reference 
is the [Couchbase tutorial](https://www.youtube.com/watch?v=61XoZZdUqhs). 

The actual code of the repository uses the Couchbase *travel-sample*.

Among other changes:
- ```cluster.enableCbas``` was removed
- I had to add code to connect to a bucket to avoid errors

## Setup

1. Container creation:

```sh
docker run -d --name cb5 -p 8091-8095:8091-8095 -p 11210:11210 couchbase:enterprise-5.5.1
```

Further instructions for analytics setup can be found [here](https://blog.couchbase.com/configuring-analytics-in-couchbase-server-5-5/).

2. Enable "Analytics" on the setup screen.
3. Create bucket
4. Create user with access to that bucket and, under "Administration &
Global Roles", "Analytics Reader" role.
5. Using the "Analytics" tab, execute the following queries:

```sql
create bucket <bucket_alias> with {"name": "<bucket source>"};
```

```sql
create shadow dataset <dataset_name> on <bucket_alias> where `type` = "<doc_type>"
```

```sql
connect bucket travel;
```

6. Validate configuration

```sql
select * from <dataset_name>;
```

## Node code

Some notes:

1. Couchbase still requires connection to a bucket
2. Analytics queries are executed against the cluster instead of bucket